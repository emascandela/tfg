//
//  Tracker.swift
//  TrackerTest
//
//  Created by Enrique Mas Candela on 23/4/18.
//  Copyright © 2018 Enrique Mas Candela. All rights reserved.
//

import Foundation
import UIKit
import CoreML

// Constants
let PADDING: Double = 0.5
let INPUT_SIZE: Int = 227
let MAX_COUNT: Int = 24

class Tracker {
    let model: goturn
    
    var first_frame: UIImage?
    var last_frame: UIImage?
    var first_target: CVImageBuffer?
    var last_bbox: [Int]?
    
    var image_height: Int = 0
    var image_width: Int = 0
    
    var count: Int = 0

    
    required init() {
        print("Loading model...")
        model = goturn()
    }
    
    func startTracking(first_frame frame: UIImage, starting_bbox bbox: [Int]) {
        last_frame = frame
        last_bbox = bbox

        count = 0
        
        image_height = Int(frame.size.height * frame.scale)
        image_width = Int(frame.size.width * frame.scale)
    }
    
    func getCrops(last_frame: UIImage, curr_frame: UIImage, last_bbox bbox: [Int]) -> ((CVPixelBuffer, CVPixelBuffer), (MLMultiArray) -> [Int]) {
        var target, image: CVPixelBuffer
        
        let bbox_w = Double(bbox[2] - bbox[0])
        let bbox_h = Double(bbox[3] - bbox[1])
        
        var region: [Int] = [
            bbox[0] - Int(PADDING * bbox_w),
            bbox[1] - Int(PADDING * bbox_h),
            bbox[2] + Int(PADDING * bbox_w),
            bbox[3] + Int(PADDING * bbox_h)
        ]

        let pad = CGFloat([-region[0], -region[1], region[2]-image_width, region[3]-image_height, 0].max()!)
        if pad > 0 {
            target = last_frame.imageWithInsets(insetDimen: pad).pixelBuffer()!
            image = curr_frame.imageWithInsets(insetDimen: pad).pixelBuffer()!
            for i in 0..<4 {
                region[i] += Int(pad)
            }
        } else {
            target = last_frame.pixelBuffer()!
            image = curr_frame.pixelBuffer()!
        }
        
        target = resizePixelBuffer(target, cropX: region[0], cropY: region[1],
                                   cropWidth: region[2]-region[0],
                                   cropHeight: region[3]-region[1],
                                   scaleWidth: INPUT_SIZE, scaleHeight: INPUT_SIZE)!
        image = resizePixelBuffer(image, cropX: region[0], cropY: region[1],
                                  cropWidth: region[2]-region[0],
                                  cropHeight: region[3]-region[1],
                                  scaleWidth: INPUT_SIZE, scaleHeight: INPUT_SIZE)!
        
        let conv = {(r_bbox: MLMultiArray) -> [Int] in
            let pad = Int(pad)
            let w: Double = Double(region[2] - region[0])
            let h: Double = Double(region[3] - region[1])
            var c_bbox: [Double] = [
                r_bbox[[NSNumber(value: 0)]].doubleValue,
                r_bbox[[NSNumber(value: 1)]].doubleValue,
                r_bbox[[NSNumber(value: 2)]].doubleValue,
                r_bbox[[NSNumber(value: 3)]].doubleValue
            ]
        
            c_bbox[0] = c_bbox[0] / 10.0 * w
            c_bbox[1] = c_bbox[1] / 10.0 * h
            c_bbox[2] = c_bbox[2] / 10.0 * w
            c_bbox[3] = c_bbox[3] / 10.0 * h
            
            let res: [Int] = [
                Int(c_bbox[0]) + region[0] - pad,
                Int(c_bbox[1]) + region[1] - pad,
                Int(c_bbox[2]) + region[0] - pad,
                Int(c_bbox[3]) + region[1] - pad
            ]
            
            return res
        }
        
        return ((target, image), conv)
    }
    
    func track(frame curr_frame: UIImage) throws -> [Int] {

        let last_frame = self.last_frame!
        let last_bbox = self.last_bbox!
        
        var target: CVPixelBuffer
        var image: CVPixelBuffer
        var conv: (MLMultiArray) -> [Int]
    
        
        ((target, image), conv) = getCrops(last_frame: last_frame, curr_frame: curr_frame, last_bbox: last_bbox)
        
        if count == 0 && first_target == nil {
            first_target = target
        }
        
        if count <= MAX_COUNT {
            count = 0
            // target = first_target!
        }
        
        let modelInput = goturnInput(target__0: target, image__0: image)
        
        let out = try self.model.prediction(input: modelInput).fc4__0
        let new_bbox = conv(out)
        
        self.last_bbox = new_bbox
        self.last_frame = curr_frame
        
        count += 1

        return new_bbox
    }
}
