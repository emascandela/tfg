//
//  ViewController.swift
//  TrackerTest
//
//  Created by Enrique Mas Candela on 6/3/18.
//  Copyright © 2018 Enrique Mas Candela. All rights reserved.
//

import UIKit
import CoreML
import Vision

class ViewController: UIViewController {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!

    var tracker: Tracker = Tracker()


    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBAction func buttonPressed() {
        DispatchQueue.global(qos: .utility).async {
            self.tracker.startTracking(first_frame: self.images[0], starting_bbox: [175, 150, 265, 240])

            for image in self.images[1...] {
                do {
                    let start = DispatchTime.now()
                    let res: [Int] = try self.tracker.track(frame: image)
                    let end = DispatchTime.now()
                    let nanoTime = end.uptimeNanoseconds - start.uptimeNanoseconds
                    let fps = 1/(Double(nanoTime) / 1e9)
                    print(fps)
                    DispatchQueue.main.async {
                        let new_image = self.drawRectangleOnImage(image: image, x: res[0], y: res[1], w: res[2]-res[0], h: res[3]-res[1])
                        self.imageView.image = new_image
                        self.label.text = String(format: "%3.2f FPS", fps)

                    }
                } catch {
                    print("ERROR")
                }
            }
        }
    }

    func drawRectangleOnImage(image: UIImage, x: Int, y: Int, w: Int, h: Int) -> UIImage {
        let imageSize = image.size
        let scale: CGFloat = 0
        UIGraphicsBeginImageContextWithOptions(imageSize, false, scale)
        
        image.draw(at: CGPoint(x: 0, y: 0))
        
        let rectangle = CGRect(x: x, y: y, width: w, height: h)
        
        UIColor.red.setStroke()
        UIRectFrame(rectangle)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    var images: [UIImage] = [ #imageLiteral(resourceName: "00000169.jpg"), #imageLiteral(resourceName: "00000170.jpg"), #imageLiteral(resourceName: "00000171.jpg"), #imageLiteral(resourceName: "00000172.jpg"), #imageLiteral(resourceName: "00000173.jpg"), #imageLiteral(resourceName: "00000174.jpg"), #imageLiteral(resourceName: "00000175.jpg"), #imageLiteral(resourceName: "00000176.jpg"), #imageLiteral(resourceName: "00000177.jpg"), #imageLiteral(resourceName: "00000178.jpg"), #imageLiteral(resourceName: "00000179.jpg"), #imageLiteral(resourceName: "00000180.jpg"), #imageLiteral(resourceName: "00000181.jpg"), #imageLiteral(resourceName: "00000182.jpg"), #imageLiteral(resourceName: "00000183.jpg"), #imageLiteral(resourceName: "00000184.jpg"), #imageLiteral(resourceName: "00000185.jpg"), #imageLiteral(resourceName: "00000186.jpg"), #imageLiteral(resourceName: "00000187.jpg"), #imageLiteral(resourceName: "00000188.jpg"), #imageLiteral(resourceName: "00000189.jpg"), #imageLiteral(resourceName: "00000190.jpg"), #imageLiteral(resourceName: "00000191.jpg"), #imageLiteral(resourceName: "00000192.jpg"), #imageLiteral(resourceName: "00000193.jpg"), #imageLiteral(resourceName: "00000194.jpg"), #imageLiteral(resourceName: "00000195.jpg"), #imageLiteral(resourceName: "00000196.jpg"), #imageLiteral(resourceName: "00000197.jpg"), #imageLiteral(resourceName: "00000198.jpg"), #imageLiteral(resourceName: "00000199.jpg"), #imageLiteral(resourceName: "00000200.jpg"), #imageLiteral(resourceName: "00000201.jpg"), #imageLiteral(resourceName: "00000202.jpg"), #imageLiteral(resourceName: "00000203.jpg"), #imageLiteral(resourceName: "00000204.jpg"), #imageLiteral(resourceName: "00000205.jpg"), #imageLiteral(resourceName: "00000206.jpg"), #imageLiteral(resourceName: "00000207.jpg"), #imageLiteral(resourceName: "00000208.jpg"), #imageLiteral(resourceName: "00000209.jpg"), #imageLiteral(resourceName: "00000210.jpg"), #imageLiteral(resourceName: "00000211.jpg"), #imageLiteral(resourceName: "00000212.jpg"), #imageLiteral(resourceName: "00000213.jpg"), #imageLiteral(resourceName: "00000214.jpg"), #imageLiteral(resourceName: "00000215.jpg"), #imageLiteral(resourceName: "00000216.jpg"), #imageLiteral(resourceName: "00000217.jpg"), #imageLiteral(resourceName: "00000218.jpg"), #imageLiteral(resourceName: "00000219.jpg"), #imageLiteral(resourceName: "00000220.jpg"), #imageLiteral(resourceName: "00000221.jpg"), #imageLiteral(resourceName: "00000222.jpg"), #imageLiteral(resourceName: "00000223.jpg"), #imageLiteral(resourceName: "00000224.jpg"), #imageLiteral(resourceName: "00000225.jpg"), #imageLiteral(resourceName: "00000226.jpg"), #imageLiteral(resourceName: "00000227.jpg"), #imageLiteral(resourceName: "00000228.jpg"), #imageLiteral(resourceName: "00000229.jpg"), #imageLiteral(resourceName: "00000230.jpg"), #imageLiteral(resourceName: "00000231.jpg"), #imageLiteral(resourceName: "00000232.jpg"), #imageLiteral(resourceName: "00000233.jpg"), #imageLiteral(resourceName: "00000234.jpg"), #imageLiteral(resourceName: "00000235.jpg"), #imageLiteral(resourceName: "00000236.jpg"), #imageLiteral(resourceName: "00000237.jpg"), #imageLiteral(resourceName: "00000238.jpg"), #imageLiteral(resourceName: "00000239.jpg"), #imageLiteral(resourceName: "00000240.jpg"), #imageLiteral(resourceName: "00000241.jpg"), #imageLiteral(resourceName: "00000242.jpg"), #imageLiteral(resourceName: "00000243.jpg"), #imageLiteral(resourceName: "00000244.jpg"), #imageLiteral(resourceName: "00000245.jpg"), #imageLiteral(resourceName: "00000246.jpg"), #imageLiteral(resourceName: "00000247.jpg"), #imageLiteral(resourceName: "00000248.jpg"), #imageLiteral(resourceName: "00000249.jpg"), #imageLiteral(resourceName: "00000250.jpg"), #imageLiteral(resourceName: "00000251.jpg"), #imageLiteral(resourceName: "00000252.jpg"), #imageLiteral(resourceName: "00000253.jpg"), #imageLiteral(resourceName: "00000254.jpg"), #imageLiteral(resourceName: "00000255.jpg"), #imageLiteral(resourceName: "00000256.jpg"), #imageLiteral(resourceName: "00000257.jpg"), #imageLiteral(resourceName: "00000258.jpg"), #imageLiteral(resourceName: "00000259.jpg"), #imageLiteral(resourceName: "00000260.jpg"), #imageLiteral(resourceName: "00000261.jpg"), #imageLiteral(resourceName: "00000262.jpg"), #imageLiteral(resourceName: "00000263.jpg"), #imageLiteral(resourceName: "00000264.jpg"), #imageLiteral(resourceName: "00000265.jpg"), #imageLiteral(resourceName: "00000266.jpg"), #imageLiteral(resourceName: "00000267.jpg"), #imageLiteral(resourceName: "00000268.jpg"), #imageLiteral(resourceName: "00000269.jpg"), #imageLiteral(resourceName: "00000270.jpg"), #imageLiteral(resourceName: "00000271.jpg"), #imageLiteral(resourceName: "00000272.jpg"), #imageLiteral(resourceName: "00000273.jpg"), #imageLiteral(resourceName: "00000274.jpg"), #imageLiteral(resourceName: "00000275.jpg"), #imageLiteral(resourceName: "00000276.jpg"), #imageLiteral(resourceName: "00000277.jpg"), #imageLiteral(resourceName: "00000278.jpg"), #imageLiteral(resourceName: "00000279.jpg"), #imageLiteral(resourceName: "00000280.jpg"), #imageLiteral(resourceName: "00000281.jpg"), #imageLiteral(resourceName: "00000282.jpg"), #imageLiteral(resourceName: "00000283.jpg"), #imageLiteral(resourceName: "00000284.jpg"), #imageLiteral(resourceName: "00000285.jpg"), #imageLiteral(resourceName: "00000286.jpg"), #imageLiteral(resourceName: "00000287.jpg"), #imageLiteral(resourceName: "00000288.jpg"), #imageLiteral(resourceName: "00000289.jpg"), #imageLiteral(resourceName: "00000290.jpg"), #imageLiteral(resourceName: "00000291.jpg"), #imageLiteral(resourceName: "00000292.jpg"), #imageLiteral(resourceName: "00000293.jpg"), #imageLiteral(resourceName: "00000294.jpg"), #imageLiteral(resourceName: "00000295.jpg"), #imageLiteral(resourceName: "00000296.jpg"), #imageLiteral(resourceName: "00000297.jpg"), #imageLiteral(resourceName: "00000298.jpg"), #imageLiteral(resourceName: "00000299.jpg"), #imageLiteral(resourceName: "00000300.jpg"), #imageLiteral(resourceName: "00000301.jpg"), #imageLiteral(resourceName: "00000302.jpg")]

    override func viewDidLoad() {
        super.viewDidLoad()

        //target.image = , #imageLiteral(resourceName: "target.jpg")
        /*
        let img = , #imageLiteral(resourceName: "full_image")
        //image.image = img
        let tracker = Tracker()
        tracker.startTracking(first_frame: img, starting_bbox: [175, 150, 265, 240])
        let ((i, t), conv) = tracker.getCrops(last_frame: img, curr_frame: img, last_bbox: [175, 150, 265, 240])
        do {
            let res = try tracker.track(frame: img)
            print(res)

        } catch {}
        
        image.image = UIImage(pixelBuffer: i)
        target.image = UIImage(pixelBuffer: t)
        */
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
}

