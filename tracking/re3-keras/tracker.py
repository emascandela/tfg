import cv2
import time
import numpy as np

from utils import im_util, bb_util
from model import network

LSTM_SIZE = 1024
CROP_SIZE = 227
CROP_PAD = 2
MAX_TRACE_LENGTH = 32
SPEED_OUTPUT = True

IMAGENET_MEAN = np.array([123.151630838, 115.902882574, 103.062623801], dtype='float32')

class Tracker():
    def __init__(self):
        self.past_bbox = None
        self.lstm_state = None
        self.prev_image = None
        self.forward_count = None
        self.total_forward_count = 0
        self.original_features = None
        self.time = 0
        model, self.lstm1, self.lstm2 = network.build_net(return_lstm=True)
        model.summary()
        self.model = network.load_model_weights(model)
        
    def track(self, image, starting_box=None):
        start_time = time.time()
        
        if type(image) == str:
            image = cv2.imread(image)[:,:,::-1]
        else:
            image = image.copy()
        
        original_image = image.copy()
            
        image_read_time = time.time() - start_time

        # if starting_box != None -> start new tracking
        if starting_box is not None:
            self.past_bbox = np.array(starting_box)
            self.lstm_state = [np.zeros((1, 1024)) for _ in range(4)]
            self.prev_image = image
            self.forward_count = 0
            self.original_features = None
        else:
            if self.past_bbox is None:
                print("Error, no starting box specified")
                return
            
        cropped_input0, past_bbox_padded = im_util.get_cropped_input(self.prev_image,
                                                                     self.past_bbox,
                                                                     CROP_PAD,
                                                                     CROP_SIZE)
        cropped_input1, _ = im_util.get_cropped_input(image,
                                                      self.past_bbox,
                                                      CROP_PAD,
                                                      CROP_SIZE)
        
        self.lstm1.reset_states(states=self.lstm_state[:2])
        self.lstm2.reset_states(states=self.lstm_state[2:])
    
        raw_output, s11, s12, s21, s22 = self.model.predict(np.array(np.reshape([cropped_input0, cropped_input1], (1, -1, 3)))-IMAGENET_MEAN)
        
        self.lstm_state = [s11, s12, s21, s22]

        print(raw_output)
        print(self.lstm_state)
        
        if self.forward_count == 0:
            self.original_features = [s11, s12, s21, s22]
            
        self.prev_image = image.copy()
        
        output_bbox = bb_util.from_crop_coordinate_system(raw_output.squeeze() / 10.0,
                                                          past_bbox_padded, 1, 1)
        
        if self.forward_count > 0 and self.forward_count % MAX_TRACE_LENGTH == 0:
            cropped_input, _ = im_util.get_cropped_input(image,
                                                         output_bbox,
                                                         CROP_PAD,
                                                         CROP_SIZE)
            
            inp = np.tile(cropped_input[np.newaxis, ...], (2,1,1,1))
            
            self.lstm1.reset_states(states=(self.original_features[0], self.original_features[1]))
            self.lstm2.reset_states(states=(self.original_features[2], self.original_features[3]))

            raw_output, s11, s12, s21, s22 = self.model.predict(np.array(np.reshape(inp, (1, -1, 3)))-IMAGENET_MEAN)
            s11, s12, s21, s22 = s11, s12, s21, s22

            self.lstm_state = [s11, s12, s21, s22]

        self.forward_count += 1
        self.total_forward_count += 1
        
        if starting_box is not None:
            output_bbox = np.array(starting_box)

        self.past_bbox = output_bbox

        end_time = time.time()
        
        if self.total_forward_count > 0:
            self.time += (end_time - start_time - image_read_time)
            
        if SPEED_OUTPUT and self.total_forward_count % 100 == 0:
            print('Current tracking speed:   %.3f FPS' % (2 * 1 / (end_time - start_time - image_read_time)))
            print('Current image read speed: %.3f FPS' % (1 / (image_read_time)))
            print('Mean tracking speed:      %.3f FPS\n' % (2 * self.total_forward_count / max(.00001, self.time)))
            
        return output_bbox
