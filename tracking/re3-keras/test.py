import os
import glob
import keras
import cv2
import numpy as np

import tracker

        
if __name__ == '__main__':
    t = tracker.Tracker()
    data_dir = 'data/'
    bbox = [175.00, 154.00, 251.00, 229.00]

    files = sorted(glob.glob(data_dir + '/*.jpg'))
    t.track(files[0], starting_box=bbox)

    for f in files:
        image = cv2.imread(f)
        imageRGB = image[:,:,::-1]
        
        bbox = t.track(imageRGB)
        print(bbox)
        cv2.rectangle(image,
                (int(bbox[0]), int(bbox[1])),
                (int(bbox[2]), int(bbox[3])),
                [0, 0, 255], 2)
        cv2.imwrite('out/'+f.split('/')[1], image)
