import numpy as np
from keras.models import Model
from keras.layers import Dense, Conv2D, Activation, MaxPooling2D, Input, Flatten, Concatenate, Permute, Lambda, LSTM, Reshape, RNN
from keras.layers.advanced_activations import PReLU
import keras.backend as K

from model.custom_layers import LRN2D, CaffeLSTMCell, BatchReshape

def build_net(return_lstm=True):
    input_shape = (1, 227*227*2, 3)

    input_layer = Input(batch_shape=input_shape)

    #div_batch = BatchReshape((2, 227, 227, 3))(input_layer)
    div_batch = Lambda(lambda x : K.reshape(x, (2, 227, 227, 3)), name='div_batch')(input_layer)
    
    conv1 = Conv2D(name='conv1', filters=96, kernel_size=(11, 11), strides=(4, 4), padding='valid', activation='relu')(div_batch)
    pool1 = MaxPooling2D(name='pool1', pool_size=(3, 3), strides=(2, 2), padding='valid')(conv1)
    lrn1 = LRN2D(name='norm1')(pool1)

    lrn1_1 = Lambda(lambda x : x[:, :, :, :48], name='split1l')(lrn1)
    lrn1_2 = Lambda(lambda x : x[:, :, :, 48:], name='split1r')(lrn1)

    conv2_g1 = Conv2D(name='conv2_g1', filters=128, kernel_size=(5, 5), strides=(1, 1), padding='same', activation='relu')(lrn1_1)
    conv2_g2 = Conv2D(name='conv2_g2', filters=128, kernel_size=(5, 5), strides=(1, 1), padding='same', activation='relu')(lrn1_2)
    conv2 = Concatenate(name='conv2', axis=3)([conv2_g1, conv2_g2])
    pool2 = MaxPooling2D(name='pool2', pool_size=(3, 3), strides=(2, 2), padding='valid')(conv2)
    lrn2 = LRN2D(name='norm2')(pool2)

    conv3 = Conv2D(name='conv3', filters=384, kernel_size=(3, 3), padding='same', activation='relu')(lrn2)

    conv3_1 = Lambda(lambda x : x[:, :, :, :192], name='split2l')(conv3)
    conv3_2 = Lambda(lambda x : x[:, :, :, 192:], name='split2r')(conv3)

    conv4_g1 = Conv2D(name='conv4_g1', filters=192, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu')(conv3_1)
    conv4_g2 = Conv2D(name='conv4_g2', filters=192, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu')(conv3_2)
    conv4 = Concatenate(name='conv4', axis=3)([conv4_g1, conv4_g2])

    conv4_1 = Lambda(lambda x : x[:, :, :, :192], name='split3l')(conv4)
    conv4_2 = Lambda(lambda x : x[:, :, :, 192:], name='split3r')(conv4)

    conv5_g1 = Conv2D(name='conv5_g1', filters=128, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu')(conv4_1)
    conv5_g2 = Conv2D(name='conv5_g2', filters=128, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu')(conv4_2)
    conv5 = Concatenate(name='conv5', axis=3)([conv5_g1, conv5_g2])
    pool5 = MaxPooling2D(name='pool5', pool_size=(3, 3), strides=(2, 2), padding='valid')(conv5)
    pool5 = Permute((3, 1, 2))(pool5)
    pool5_flat = Flatten()(pool5)


    conv1_skip = Conv2D(name='conv1_skip', filters=16, kernel_size=(1, 1))(lrn1)
    conv1_skip_prelu = PReLU(name='conv1_skip_prelu', shared_axes=[1, 2])(conv1_skip)
    conv1_skip_perm = Permute((3, 1, 2))(conv1_skip_prelu)
    conv1_skip_flat = Flatten()(conv1_skip_perm) # Correct?

    conv2_skip = Conv2D(name='conv2_skip', filters=32, kernel_size=(1, 1))(lrn2)
    conv2_skip = PReLU(name='conv2_skip_prelu', shared_axes=[1, 2])(conv2_skip)
    conv2_skip = Permute((3, 1, 2))(conv2_skip)
    conv2_skip_flat = Flatten()(conv2_skip)

    conv5_skip = Conv2D(name='conv5_skip', filters=64, kernel_size=(1, 1))(conv5)
    conv5_skip = PReLU(name='conv5_skip_prelu', shared_axes=[1, 2])(conv5_skip)
    conv5_skip = Permute((3, 1, 2))(conv5_skip)
    conv5_skip_flat = Flatten()(conv5_skip)

    big_concat = Concatenate(name='big_concat', axis=1)([conv1_skip_flat, conv2_skip_flat, conv5_skip_flat, pool5_flat])
    big_concat = Lambda(lambda x: K.reshape(x, (1, 1, 74208)), name='flatten_batch')(big_concat)

    ######

    fc6_out = Dense(2048, name='fc6', activation='relu')(big_concat)
    print(fc6_out.shape)
    
    lstm1_cell = CaffeLSTMCell(1024)
    lstm2_cell = CaffeLSTMCell(1024)
    lstm1 = RNN(lstm1_cell, name='lstm1', stateful=True, return_state=True, return_sequences=True)
    lstm2 = RNN(lstm2_cell, name='lstm2', stateful=True, return_state=True, return_sequences=True)
    
    lstm1_out, s11, s12 = lstm1(fc6_out)
    
    lstm1_fc6 = Concatenate(axis=1)([Reshape((2048, 1))(fc6_out),
                                     Reshape((1024, 1))(lstm1_out)])
    lstm1_fc6 = Reshape((1, 3072))(lstm1_fc6)
    lstm2_out, s21, s22 = lstm2(lstm1_fc6)

    lstm2_out = Flatten()(lstm2_out)

    fc_output = Dense(4, name='fc_out')(lstm2_out)


    if return_lstm:
        model = Model(input_layer, (fc_output, s11, s12, s21, s22))
        return model, lstm1, lstm2
    else:
        model = Model(input_layer, fc_output)
        return model
    
    return Model(input_layer, big_concat)
    
def build_model(return_lstm=True):
    input_shape = (1, 1, 74208)
    
    inp = Input(batch_shape=input_shape) # , dtype='float64')
    
    fc6_out = Dense(2048, name='fc6', activation='relu')(inp)
    
    lstm1_cell = CaffeLSTMCell(1024)
    lstm2_cell = CaffeLSTMCell(1024)
    lstm1 = RNN(lstm1_cell, name='lstm1', stateful=True, return_state=True, return_sequences=True)
    lstm2 = RNN(lstm2_cell, name='lstm2', stateful=True, return_state=True, return_sequences=True)
    
    lstm1_out, s11, s12 = lstm1(fc6_out)
    
    lstm1_fc6 = Concatenate(axis=1)([Reshape((2048, 1))(fc6_out),
                                     Reshape((1024, 1))(lstm1_out)])
    lstm1_fc6 = Reshape((1, 3072))(lstm1_fc6)
    lstm2_out, s21, s22 = lstm2(lstm1_fc6)

    lstm2_out = Flatten()(lstm2_out)

    fc_output = Dense(4, name='fc_out')(lstm2_out)


    if return_lstm:
        model = Model(inp, (fc_output, s11, s12, s21, s22))
        return model, lstm1, lstm2
    else:
        model = Model(inp, fc_output)
        return model

def load_model_weights(model, directory='tf-weights/'):
    conv1_W = np.load(directory+'re3_conv1_W_conv.npy')
    conv1_b = np.load(directory+'re3_conv1_b_conv.npy')
    conv1_skip_W = np.load(directory+'re3_conv1_skip_W_conv.npy')
    conv1_skip_b = np.load(directory+'re3_conv1_skip_b_conv.npy')
    conv1_skip_prelu = np.load(directory+'re3_conv1_skip_prelu.npy')
    conv2_W = np.load(directory+'re3_conv2_W_conv.npy')
    conv2_b = np.load(directory+'re3_conv2_b_conv.npy')
    conv2_skip_W = np.load(directory+'re3_conv2_skip_W_conv.npy')
    conv2_skip_b = np.load(directory+'re3_conv2_skip_b_conv.npy')
    conv2_skip_prelu = np.load(directory+'re3_conv2_skip_prelu.npy')
    conv3_W = np.load(directory+'re3_conv3_W_conv.npy')
    conv3_b = np.load(directory+'re3_conv3_b_conv.npy')
    conv4_W = np.load(directory+'re3_conv4_W_conv.npy')
    conv4_b = np.load(directory+'re3_conv4_b_conv.npy')
    conv5_W = np.load(directory+'re3_conv5_W_conv.npy')
    conv5_b = np.load(directory+'re3_conv5_b_conv.npy')
    conv5_skip_W = np.load(directory+'re3_conv5_skip_W_conv.npy')
    conv5_skip_b = np.load(directory+'re3_conv5_skip_b_conv.npy')
    conv5_skip_prelu = np.load(directory+'re3_conv5_skip_prelu.npy')
    fc6_W = np.load(directory+'re3_fc6_W_fc.npy')
    fc6_b = np.load(directory+'re3_fc6_b_fc.npy')
    fc_out_W = np.load(directory+'re3_fc_output_W_fc.npy')
    fc_out_b = np.load(directory+'re3_fc_output_b_fc.npy')
    
    lstm_1_bi_b = np.load(directory+'re3_lstm1_rnn_LSTM_block_input_biases.npy')
    lstm_1_bi_W = np.load(directory+'re3_lstm1_rnn_LSTM_block_input_weights.npy')
    lstm_1_fg_b = np.load(directory+'re3_lstm1_rnn_LSTM_forget_gate_biases.npy')
    lstm_1_fg_W = np.load(directory+'re3_lstm1_rnn_LSTM_forget_gate_weights.npy')
    lstm_1_ig_b = np.load(directory+'re3_lstm1_rnn_LSTM_input_gate_biases.npy')
    lstm_1_ig_W = np.load(directory+'re3_lstm1_rnn_LSTM_input_gate_weights.npy')
    lstm_1_og_b = np.load(directory+'re3_lstm1_rnn_LSTM_output_gate_biases.npy')
    lstm_1_og_W = np.load(directory+'re3_lstm1_rnn_LSTM_output_gate_weights.npy')
    
    lstm_2_bi_b = np.load(directory+'re3_lstm2_rnn_LSTM_block_input_biases.npy')
    lstm_2_bi_W = np.load(directory+'re3_lstm2_rnn_LSTM_block_input_weights.npy')
    lstm_2_fg_b = np.load(directory+'re3_lstm2_rnn_LSTM_forget_gate_biases.npy')
    lstm_2_fg_W = np.load(directory+'re3_lstm2_rnn_LSTM_forget_gate_weights.npy')
    lstm_2_ig_b = np.load(directory+'re3_lstm2_rnn_LSTM_input_gate_biases.npy')
    lstm_2_ig_W = np.load(directory+'re3_lstm2_rnn_LSTM_input_gate_weights.npy')
    lstm_2_og_b = np.load(directory+'re3_lstm2_rnn_LSTM_output_gate_biases.npy')
    lstm_2_og_W = np.load(directory+'re3_lstm2_rnn_LSTM_output_gate_weights.npy')
    
    model_names = [l.name for l in model.layers]

    model.layers[model_names.index('conv1')].set_weights([conv1_W, conv1_b])

    model.layers[model_names.index('conv1_skip')].set_weights([conv1_skip_W, conv1_skip_b])
    model.layers[model_names.index('conv1_skip_prelu')].set_weights(
        [np.ones(model.layers[model_names.index('conv1_skip_prelu')].get_weights()[0].shape) * conv1_skip_prelu])

    model.layers[model_names.index('conv2_g1')].set_weights([conv2_W[:, :, :, :128], conv2_b[:128]])
    model.layers[model_names.index('conv2_g2')].set_weights([conv2_W[:, :, :, 128:], conv2_b[128:]])

    model.layers[model_names.index('conv2_skip')].set_weights([conv2_skip_W, conv2_skip_b])
    model.layers[model_names.index('conv2_skip_prelu')].set_weights(
        [np.ones(model.layers[model_names.index('conv2_skip_prelu')].get_weights()[0].shape) * conv2_skip_prelu])

    model.layers[model_names.index('conv3')].set_weights([conv3_W, conv3_b])

    model.layers[model_names.index('conv4_g1')].set_weights([conv4_W[:, :, :, :192], conv4_b[:192]])
    model.layers[model_names.index('conv4_g2')].set_weights([conv4_W[:, :, :, 192:], conv4_b[192:]])

    model.layers[model_names.index('conv5_g1')].set_weights([conv5_W[:, :, :, :128], conv5_b[:128]])
    model.layers[model_names.index('conv5_g2')].set_weights([conv5_W[:, :, :, 128:], conv5_b[128:]])

    model.layers[model_names.index('conv5_skip')].set_weights([conv5_skip_W, conv5_skip_b])
    model.layers[model_names.index('conv5_skip_prelu')].set_weights(
        [np.ones(model.layers[model_names.index('conv5_skip_prelu')].get_weights()[0].shape) * conv5_skip_prelu])

    model.layers[model_names.index('fc6')].set_weights([fc6_W, fc6_b])
    model.layers[model_names.index('fc_out')].set_weights([fc_out_W, fc_out_b])
    
    model.layers[model_names.index('lstm1')].set_weights([lstm_1_bi_W, lstm_1_bi_b,
                                                          lstm_1_ig_W, lstm_1_ig_b,
                                                          lstm_1_fg_W, lstm_1_fg_b,
                                                          lstm_1_og_W, lstm_1_og_b])
    
    model.layers[model_names.index('lstm2')].set_weights([lstm_2_bi_W, lstm_2_bi_b,
                                                          lstm_2_ig_W, lstm_2_ig_b,
                                                          lstm_2_fg_W, lstm_2_fg_b,
                                                          lstm_2_og_W, lstm_2_og_b])
    return model


def load_weights(base, model, directory='tf-weights/'):
    conv1_W = np.load(directory+'re3_conv1_W_conv.npy')
    conv1_b = np.load(directory+'re3_conv1_b_conv.npy')
    conv1_skip_W = np.load(directory+'re3_conv1_skip_W_conv.npy')
    conv1_skip_b = np.load(directory+'re3_conv1_skip_b_conv.npy')
    conv1_skip_prelu = np.load(directory+'re3_conv1_skip_prelu.npy')
    conv2_W = np.load(directory+'re3_conv2_W_conv.npy')
    conv2_b = np.load(directory+'re3_conv2_b_conv.npy')
    conv2_skip_W = np.load(directory+'re3_conv2_skip_W_conv.npy')
    conv2_skip_b = np.load(directory+'re3_conv2_skip_b_conv.npy')
    conv2_skip_prelu = np.load(directory+'re3_conv2_skip_prelu.npy')
    conv3_W = np.load(directory+'re3_conv3_W_conv.npy')
    conv3_b = np.load(directory+'re3_conv3_b_conv.npy')
    conv4_W = np.load(directory+'re3_conv4_W_conv.npy')
    conv4_b = np.load(directory+'re3_conv4_b_conv.npy')
    conv5_W = np.load(directory+'re3_conv5_W_conv.npy')
    conv5_b = np.load(directory+'re3_conv5_b_conv.npy')
    conv5_skip_W = np.load(directory+'re3_conv5_skip_W_conv.npy')
    conv5_skip_b = np.load(directory+'re3_conv5_skip_b_conv.npy')
    conv5_skip_prelu = np.load(directory+'re3_conv5_skip_prelu.npy')
    fc6_W = np.load(directory+'re3_fc6_W_fc.npy')
    fc6_b = np.load(directory+'re3_fc6_b_fc.npy')
    fc_out_W = np.load(directory+'re3_fc_output_W_fc.npy')
    fc_out_b = np.load(directory+'re3_fc_output_b_fc.npy')
    
    lstm_1_bi_b = np.load(directory+'re3_lstm1_rnn_LSTM_block_input_biases.npy')
    lstm_1_bi_W = np.load(directory+'re3_lstm1_rnn_LSTM_block_input_weights.npy')
    lstm_1_fg_b = np.load(directory+'re3_lstm1_rnn_LSTM_forget_gate_biases.npy')
    lstm_1_fg_W = np.load(directory+'re3_lstm1_rnn_LSTM_forget_gate_weights.npy')
    lstm_1_ig_b = np.load(directory+'re3_lstm1_rnn_LSTM_input_gate_biases.npy')
    lstm_1_ig_W = np.load(directory+'re3_lstm1_rnn_LSTM_input_gate_weights.npy')
    lstm_1_og_b = np.load(directory+'re3_lstm1_rnn_LSTM_output_gate_biases.npy')
    lstm_1_og_W = np.load(directory+'re3_lstm1_rnn_LSTM_output_gate_weights.npy')
    
    lstm_2_bi_b = np.load(directory+'re3_lstm2_rnn_LSTM_block_input_biases.npy')
    lstm_2_bi_W = np.load(directory+'re3_lstm2_rnn_LSTM_block_input_weights.npy')
    lstm_2_fg_b = np.load(directory+'re3_lstm2_rnn_LSTM_forget_gate_biases.npy')
    lstm_2_fg_W = np.load(directory+'re3_lstm2_rnn_LSTM_forget_gate_weights.npy')
    lstm_2_ig_b = np.load(directory+'re3_lstm2_rnn_LSTM_input_gate_biases.npy')
    lstm_2_ig_W = np.load(directory+'re3_lstm2_rnn_LSTM_input_gate_weights.npy')
    lstm_2_og_b = np.load(directory+'re3_lstm2_rnn_LSTM_output_gate_biases.npy')
    lstm_2_og_W = np.load(directory+'re3_lstm2_rnn_LSTM_output_gate_weights.npy')
    
    model_names = [l.name for l in model.layers]
    base_names = [l.name for l in base.layers]

    base.layers[base_names.index('conv1')].set_weights([conv1_W, conv1_b])

    base.layers[base_names.index('conv1_skip')].set_weights([conv1_skip_W, conv1_skip_b])
    base.layers[base_names.index('conv1_skip_prelu')].set_weights(
        [np.ones(base.layers[base_names.index('conv1_skip_prelu')].get_weights()[0].shape) * conv1_skip_prelu])

    base.layers[base_names.index('conv2_g1')].set_weights([conv2_W[:, :, :, :128], conv2_b[:128]])
    base.layers[base_names.index('conv2_g2')].set_weights([conv2_W[:, :, :, 128:], conv2_b[128:]])

    base.layers[base_names.index('conv2_skip')].set_weights([conv2_skip_W, conv2_skip_b])
    base.layers[base_names.index('conv2_skip_prelu')].set_weights(
        [np.ones(base.layers[base_names.index('conv2_skip_prelu')].get_weights()[0].shape) * conv2_skip_prelu])

    base.layers[base_names.index('conv3')].set_weights([conv3_W, conv3_b])

    base.layers[base_names.index('conv4_g1')].set_weights([conv4_W[:, :, :, :192], conv4_b[:192]])
    base.layers[base_names.index('conv4_g2')].set_weights([conv4_W[:, :, :, 192:], conv4_b[192:]])

    base.layers[base_names.index('conv5_g1')].set_weights([conv5_W[:, :, :, :128], conv5_b[:128]])
    base.layers[base_names.index('conv5_g2')].set_weights([conv5_W[:, :, :, 128:], conv5_b[128:]])

    base.layers[base_names.index('conv5_skip')].set_weights([conv5_skip_W, conv5_skip_b])
    base.layers[base_names.index('conv5_skip_prelu')].set_weights(
        [np.ones(base.layers[base_names.index('conv5_skip_prelu')].get_weights()[0].shape) * conv5_skip_prelu])

    model.layers[model_names.index('fc6')].set_weights([fc6_W, fc6_b])
    model.layers[model_names.index('fc_out')].set_weights([fc_out_W, fc_out_b])
    
    model.layers[model_names.index('lstm1')].set_weights([lstm_1_bi_W, lstm_1_bi_b,
                                                          lstm_1_ig_W, lstm_1_ig_b,
                                                          lstm_1_fg_W, lstm_1_fg_b,
                                                          lstm_1_og_W, lstm_1_og_b])
    
    model.layers[model_names.index('lstm2')].set_weights([lstm_2_bi_W, lstm_2_bi_b,
                                                          lstm_2_ig_W, lstm_2_ig_b,
                                                          lstm_2_fg_W, lstm_2_fg_b,
                                                          lstm_2_og_W, lstm_2_og_b])
    return base, model
