from keras.engine.topology import Layer
import keras.backend as K

class BatchReshape(Layer):
    def __init__(self, shape, **kwargs):
        super(BatchReshape, self).__init__(**kwargs)
        self.shape = shape
        print(shape)

    def build(self, input_shape):
        super(BatchReshape, self).build(input_shape)          
        
    def call(self, x, mask=None):
        print(x.shape)
        print(self.shape)
        print(K.reshape(x, self.shape))
        return K.reshape(x, self.shape)


class LRN2D(Layer):
    def __init__(self, alpha=2e-5, k=1, beta=0.75, n=5, **kwargs):
        if n % 2 == 0:
            raise NotImplementedError("LRN2D only works with odd n. n provided: " + str(n))
        super(LRN2D, self).__init__(**kwargs)
        self.alpha = alpha
        self.k = k
        self.beta = beta
        self.n = n
        
    def build(self, input_shape):
        self.shape = input_shape
        super(LRN2D, self).build(input_shape)          

    def call(self, x, mask=None):
        _, r, c, ch = x.shape
        b = 2
        half_n = self.n // 2
        input_sqr = K.square(x)
        extra_channels = K.zeros((b, r, c, ch + 2 * half_n))
        input_sqr = K.concatenate([extra_channels[:, :, :, :half_n],
                                   input_sqr,
                                   extra_channels[:, :, :, half_n + int(ch):]],
                                  axis=3)
        
        scale = self.k
        norm_alpha = self.alpha / self.n
        for i in range(self.n):
            scale += self.alpha * input_sqr[:, :, :, i:i + int(ch)]
        scale = scale ** self.beta
        return x / scale
    
class CaffeLSTMCell(Layer):
    def __init__(self, units, **kwargs):
        self.units = units
        self.state_size = [self.units, self.units]
        super(CaffeLSTMCell, self).__init__(**kwargs)
        
    def build(self, input_shape):
        inputs_shape = input_shape[1] + self.units
        peephole_shape = inputs_shape + self.units
        
        self.block_input_W = self.add_weight(name='block_input_W', shape=[inputs_shape, self.units], initializer='zeros')
        self.block_input_b = self.add_weight(name='block_input_b', shape=[self.units], initializer='zeros')
        
        self.input_gate_W = self.add_weight(name='input_gate_W', shape=[peephole_shape, self.units], initializer='zeros')
        self.input_gate_b = self.add_weight(name='input_gate_b', shape=[self.units], initializer='zeros')
        
        self.forget_gate_W = self.add_weight(name='forget_gate_W', shape=[peephole_shape, self.units], initializer='zeros')
        self.forget_gate_b = self.add_weight(name='forget_gate_b', shape=[self.units], initializer='zeros')
        
        self.output_gate_W = self.add_weight(name='output_gate_W', shape=[peephole_shape, self.units], initializer='zeros')
        self.output_gate_b = self.add_weight(name='output_gate_b', shape=[self.units], initializer='zeros')
        
        self.built = True
        
    def call(self, inputs, states):
        cell_state_prev, cell_outputs_prev = states
        lstm_concat = K.concatenate([inputs, cell_outputs_prev], axis=1)
        peephole_concat = K.concatenate([lstm_concat, states[0]], axis=1)
        
        block_input = K.tanh(K.dot(lstm_concat, self.block_input_W) + self.block_input_b)
        
        input_gate = K.sigmoid(K.dot(peephole_concat, self.input_gate_W) + self.input_gate_b)
        input_mult = input_gate * block_input
        
        forget_gate = K.sigmoid(K.dot(peephole_concat, self.forget_gate_W) + self.forget_gate_b)
        forget_mult = forget_gate * states[0]
        
        cell_state_new = input_mult + forget_mult
        cell_state_act = K.tanh(cell_state_new)
        
        output_concat = K.concatenate([lstm_concat, cell_state_new], axis=1) 
        output_concat_shape = output_concat.get_shape().as_list()[1]
        
        output_gate = K.sigmoid(K.dot(output_concat, self.output_gate_W) + self.output_gate_b)
        cell_outputs_new = output_gate * cell_state_act
        
        new_state = (cell_state_new, cell_outputs_new)
        
        return cell_outputs_new, new_state
