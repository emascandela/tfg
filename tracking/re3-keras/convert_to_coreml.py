import coremltools
from coremltools.proto import NeuralNetwork_pb2

from model import network

if __name__ == '__main__':
    model = network.build_net(return_lstm=False)
    model = network.load_model_weights(model)


    # base = network.build_base_net()
    # model = network.build_model(return_lstm=False)
    # base, model = network.load_weights(base, model)

    def convert_lambda(layer):
        params = NeuralNetwork_pb2.CustomLayerParams()
        if layer.name == 'div_batch':
            params.className = "DivBatch"
            params.description = "Reshapes a tensor including the batch axis."
        elif layer.name == 'flatten_batch':
            params.className = "FlattenBatch"
            params.description = "Flattens a tensor including the batch axis."
        else:
            params.className = "Split"
            params.description = "Splits the tensor by half in axis 3."
            
            if layer.name == 'split1l':
                params.parameters['from'].intValue = 0
                params.parameters['to'].intValue = 48
            elif layer.name == 'split1r':
                params.parameters['from'].intValue = 48
                params.parameters['to'].intValue = 96
            elif layer.name in ['split2l', 'split3l']:
                params.parameters['from'].intValue = 0
                params.parameters['to'].intValue = 192
            elif layer.name in ['split2r', 'split3r']:
                params.parameters['from'].intValue = 192
                params.parameters['to'].intValue = 384

        return params

    def convert_LRN2D(layer):
        params = NeuralNetwork_pb2.CustomLayerParams()
        params.className = "LRN2D"
        params.description = "Local Response Normalization Layer"

        return params

    def convert_CaffeLSTM(layer):
        params = NeuralNetwork_pb2.CustomLayerParams()
        params.parameters['units'].intValue = 1024;

        for weights in layer.get_weights():
            w = params.weights.add()
            w.floatValue.extend(map(float, weights.flatten().tolist()))

        return params

    coreml_base = coremltools.converters.keras.convert(
        model,
        input_names=["image"],
        add_custom_layers=True,
        custom_conversion_functions={"Lambda": convert_lambda,
                                     "LRN2D": convert_LRN2D,
                                     "RNN": convert_CaffeLSTM}
        
    )

    coreml_base.author = "emascandela"
    coreml_base.license = "Public Domain"
    coreml_base.short_description = "RE3 tracker model"

    coreml_base.input_description["image"] = "Input image"

    coreml_base.save("Model.mlmodel")

    exit(0)

    coreml_model = coremltools.converters.keras.convert(
        model,
        input_names="input",
        output_names="output",
        add_custom_layers=True,
        custom_conversion_functions={"RNN": convert_CaffeLSTM}
    )


    coreml_model.author = "emascandela"
    coreml_model.license = "Public Domain"
    coreml_model.short_description = "Recurrent model for RE3 tracker"

    coreml_model.input_description["input"] = "Input data"
    coreml_model.output_description["output"] = "Tracker prediction"

    coreml_model.save("RecurrentModel.mlmodel")
